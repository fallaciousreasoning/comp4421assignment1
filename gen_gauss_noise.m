% GEN_GAUSS_NOISE Generate additive Gaussian noise.
%
%   Y = GEN_GAUSS_NOISE(m,n,sigma) generates an additive Gaussian noise image of
%   size m-by-n with the standard deviation of the noise equals sigma.
%
function Noise = gen_gauss_noise(sizeX, sizeY, sigma)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO 2:
% Generate white Gaussian noise with the given sigma.
%
% Noise = ?
Noise = zeros(sizeX, sizeY, 'double');
for x=1:sizeX
    for y=1:sizeY
        Noise(x,y) = normrnd(0, sigma);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure the noise is of double datatype.
assert_double_image(Noise);