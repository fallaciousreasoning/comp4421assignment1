% GRAD_MAG_IMAGE Compute a gradient magnitude image of the given image.
%
%   Y = GRAD_MAG_IMAGE(X) computes a gradient magnitude image of the image X.
%   Computation is based on the formula given in the lecture notes on image 
%   enhancement in spatial domain, pp.83.  The intensity values of the pixels 
%   that are out of the image boundary are treated as zeros.
%
%   REMINDER: The gradient magnitude image return should be in uint8 type.
%
function GMIm = grad_mag_image(Im)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

assert_grayscale_image(Im);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO 1: 
% Compute the gradient magnitude image.
% GMIm = ?;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Im = double(Im);
imSize = size(Im);
GMIm = zeros(imSize, 'double');

for x = 1:imSize(1)
    for y = 1:imSize(2)
        GMIm(x, y) = GradientMagnitudeAt(x, y, Im);
    end
end

GMIm = uint8(GMIm);
assert_uint8_image(GMIm);
end

function GM = GradientMagnitudeAt(X, Y, Im)
    imSize = size(Im);
    minPoint = [X - 1, Y - 1];
    maxPoint = [X + 1, Y + 1];
    
    if minPoint(1) < 1 || minPoint(2) < 1 || maxPoint(1) > imSize(1) || maxPoint(2) > imSize(2)
        GM = 0;
        return
    end
    
    T1 = abs((Im(X - 1, Y + 1) + 2 * Im(X, Y + 1) + Im(X + 1, Y + 1)) - (Im(X - 1, Y - 1) + 2 * Im(X, Y - 1) + Im(X + 1, Y - 1)));
    T2 = abs((Im(X + 1, Y - 1) + 2 * Im(X + 1, Y) + Im(X + 1, Y + 1)) - (Im(X - 1, Y - 1) + 2 * Im(X - 1, Y) + Im(X - 1, Y + 1)));
    GM = T1 + T2;
end